import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AuthService } from './_services/auth/auth.service';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ErrorInterceptorProvider } from './_services/error/error.interceptor';
import { AlertifyjsService } from './_services/alertify/alertifyjs.service';
import { appRoutes } from './routes';
import { AuthGuard } from './_guards/auth.guard';
import { AddPhoneComponent } from './add-phone/add-phone.component';
import { DataTableComponent } from './data-table/data-table.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { PhoneService } from './_services/phone/phone.service';


@NgModule({
   declarations: [
      AppComponent,
      NavbarComponent,
      FooterComponent,
      HomeComponent,
      RegisterComponent,
      AddPhoneComponent,
      DataTableComponent,
      FileUploadComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      BsDatepickerModule.forRoot(),
      BsDropdownModule.forRoot(),
      RouterModule.forRoot(appRoutes)
   ],
   providers: [
      AuthService,
      ErrorInterceptorProvider,
      AlertifyjsService,
      AuthGuard,
      PhoneService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
