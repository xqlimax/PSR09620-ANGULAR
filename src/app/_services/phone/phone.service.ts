import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhoneService {
  baseUrl = 'http://localhost:5000/api/phone/';

constructor(private http: HttpClient) { }

addPhone(model: any) {
  return this.http.post(this.baseUrl + 'add', model);
}

}

