import {Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { DataTableComponent } from './data-table/data-table.component';
import { AddPhoneComponent } from './add-phone/add-phone.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

export const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {
        path: '',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [
            {path: 'data-table', component: DataTableComponent},
            {path: 'add-phone', component: AddPhoneComponent},
            {path: 'file-upload', component: FileUploadComponent}
        ]
    },
    {path: '**', redirectTo: '', pathMatch: 'full'}
];
