import { Component, OnInit } from '@angular/core';
import { PhoneService } from '../_services/phone/phone.service';
import { AlertifyjsService } from '../_services/alertify/alertifyjs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-phone',
  templateUrl: './add-phone.component.html',
  styleUrls: ['./add-phone.component.css']
})
export class AddPhoneComponent implements OnInit {
  model: any = {};

  constructor(private phoneService: PhoneService, private alertify: AlertifyjsService,
    private router: Router) { }

  ngOnInit() {
  }

  addPhone()
  {
    this.phoneService.addPhone(this.model).subscribe(() => {
      this.alertify.success('Phone added');
      this.router.navigate(['/add-phone']);
    },
    error => {
      this.alertify.error(error);
    });
  }
}
