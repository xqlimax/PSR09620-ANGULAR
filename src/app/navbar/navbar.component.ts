import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth/auth.service';
import { AlertifyjsService } from '../_services/alertify/alertifyjs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  model: any = {};
  constructor(public authService: AuthService, private alertify: AlertifyjsService,
    private router: Router) { }


  ngOnInit() {
  }
  login() {
    this.authService.login(this.model).subscribe(next => {
      this.alertify.success('Logged in successfully');
    }, error => {
      this.alertify.error(error);
    },
    () => {
      this.router.navigate(['/data_table']);
    });
  }

  loggedIn() {
return this.authService.loggedIn();
}

logout() {
  localStorage.removeItem('token');
  this.alertify.message('Logged out successfully');
  this.router.navigate(['/home']);
}
}
